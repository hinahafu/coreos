FROM	xosamupuse/base:master

COPY    . /lib

ARG     build_env
ENV     build_env ${build_env}
RUN     sh /lib/bin/ci.sh

WORKDIR		/build
